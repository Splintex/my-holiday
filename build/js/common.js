(function() {
	'use strict';
	$(document).click(function(){
		if ($(".js-search").hasClass("is-active")) {
			$(".js-toggle-search").removeClass("is-active");
			$(".js-search").removeClass("is-active");
		}
		if ($(".js-dropdown").hasClass("is-active")) {
			$(".js-dropdown-trigger").removeClass("is-active");
			$(".js-dropdown").removeClass("is-active");
		}
		if ($(".sssl").hasClass("is-active")) {
			
			$(".sssl").removeClass("is-active");
		}
	});
// show/hide any blocks
	$(".js-toggle").on("click", function() {
		var el = $(this).attr("data-toggle");
		$("."+el).toggleClass("is-active");
		$(this).toggleClass("is-active");
		return false;
	});

// search
	$(".js-toggle-search").on("click", function(event) {
		$(this).parents(".js-search").toggleClass("is-active");
		$(this).parents(".js-search").find(".input").trigger("click");
		event.stopPropagation();
		return false;
	});
	$(".js-search .search-form__main").on("click", function(event) {
		event.stopPropagation();
	});

	$(".js-search .search__main").on("click", function(event) {
		event.stopPropagation();
	});

// dropdown
	$(".js-dropdown-trigger").on("click", function(event) {
		var dropdown = $(this).attr("data-dropdown");
		if ($(this).hasClass("is-active")) {
			$(this).removeClass("is-active");
			$("."+dropdown).removeClass("is-active");
		}
		else {
			$(".js-dropdown-trigger").removeClass("is-active");
			$(".js-dropdown").removeClass("is-active");
			$(this).addClass("is-active");
			$("."+dropdown).addClass("is-active");
		}
		event.stopPropagation();
		return false;
	});
	$(".js-dropdown").on("click", function(event) {
		event.stopPropagation();
	});

	$(".js-dropdown").on("click", function(event) {
		event.stopPropagation();
	});

// remove parent
	$(".js-del").on("click", function(event) {
		$(this).parents(".js-parent").remove();
		return false;
	});

// select 2 init
	$(".js-select").select2();
// select 2 with json
	$('.js-select-json').select2({
		minimumInputLength: 1,
		ajax: {
			type: 'post',
			url: 'url-to-your-json-file/file.json',
			dataType: 'json',
			minimumInputLength: 1,
			data: function (term, page) {
				return {
					q: term
				};
			},
			results: function (data, page) {
				return { results: data };
			}
		}
	});

// // popup select
	var Select = {
		init: function(options) {
			this._el = document.querySelectorAll(options.selector);
			this._parentClass = options.cssClass;
			this._body = document.body;
			this._makeDom();
			this._firstLoad();
			this._onClick();
		},
		_makeDom: function() {
			this._createParent();
			this._createList();
			this._createTitle();
		},
		_createParent: function() {
			for (var i = 0; i < this._el.length; i++) {
				var parent = document.createElement("div");
				this._el[i].parentNode.insertBefore(parent, this._el[i]);
				parent.appendChild(this._el[i])
				parent.classList.add(this._parentClass);
				parent.classList.add("sssl");
			}
			this._parent = document.querySelectorAll("." + this._parentClass);
		},
		_createList: function() {
			for (var i = 0; i < this._parent.length; i++) {
				var options = this._parent[i].querySelectorAll("option");
				var ul = document.createElement('ul');
				for (var j = 0; j < options.length; j++) {
					var li = "<li>" + options[j].innerHTML + "</li>";
					ul.insertAdjacentHTML("beforeEnd", li);
				}
				this._parent[i].appendChild(ul);
				ul.classList.add(this._parentClass + "__list");
			}
			this._ul = document.querySelectorAll(this._parentClass + " ul");

		},
		_createTitle: function() {
			for (var i = 0; i < this._parent.length; i++) {
				var title = document.createElement("span");
				title.classList.add(this._parentClass + "__head");
				this._parent[i].appendChild(title);
			}
			this._title = document.querySelectorAll(this._parentClass + " span");
		},
		_onClick: function() {
			for (var i = 0; i < this._parent.length; i++) {
				this._parent[i].addEventListener("click", this._clickAction.bind(this));
			}
		},
		_clickAction: function(event) {
			if (event.target.tagName == "SPAN") {
				this._toggleList(event);
			}
			if (event.target.tagName == "LI") {
				this._changeVal(event);
			}
		},
		_toggleList: function(event) {
			event.currentTarget.classList.toggle("is-active");
		},
		_changeVal: function(event) {
			var arrLi = event.currentTarget.querySelectorAll("li");
			var arrOptions = event.currentTarget.querySelectorAll("option");
			var target = event.target;
			var parent = event.currentTarget;
			var index = [].indexOf.call(arrLi, target);
			for (var j = 0; j < arrOptions.length; j++) {
				if (index == j) {
					arrOptions[j].selected = true;
					break;
				}
			}
			this._changeTitle(parent, arrLi[index].innerHTML);
			this._toggleList(event);
		},
		_changeTitle: function(parent, text) {
			parent.querySelector("span").innerHTML = text;
		},
		_firstLoad: function() {
			for (var i = 0; i < this._parent.length; i++) {
				var options = this._parent[i].querySelectorAll("option");
				var li = this._parent[i].querySelectorAll("li");
				var arrLi = [].slice.call(li);
				var title = this._parent[i].querySelector("span");
				for (var j = 0; j < options.length; j++) {
					var option = options[j];
					if (option.selected) {
						var optionIndex = j;
						break;
					}
				}
				title.innerHTML = arrLi[optionIndex].innerHTML;
			}
		}
	}
	Select.init({
		selector: ".js-select-popup",
		cssClass: "sssl"
	});

	$(".sssl").on("click", function(event) {
		event.stopPropagation();
	})


// header extend
	$(".js-toggle-header").on("click", function() {
		$(".header").toggleClass("is-active");
		$(this).toggleClass("is-active");
		return false;
	});

// menu
	$(".js-toggle-menu").on("click", function() {
		$(".js-menu").toggleClass("is-active");
		$(".js-toggle-menu").toggleClass("is-active");
		return false;
	});

	$('.js-slider').on('init', function(slick) {
		setTimeout(function(){
			$('.js-slider').parent().addClass("is-ready");
		},200);
	});
	$(".js-slider-promo").on('init', function(slick) {
		setTimeout(function(){
			$(".js-slider-promo").addClass("is-ready");
		},200);
	});
	$('.js-img-gallery').on('init', function(slick) {
		setTimeout(function(){
			$('.js-img-gallery').parents('.img-gallery').addClass("is-ready");
		},200);
	});
	$(".js-slider").slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '80px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 460,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '60px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 375,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$(".js-slider-promo").slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: true,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 940,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$(".js-img-gallery").slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		responsive: [
			{
				breakpoint: 5000,
				settings: "unslick"
			},
			{
				breakpoint: 940,
				settings: {
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: true,
				}
			},
		]
	});
			
	$(".js-prev").on("click", function() {
		$(this).parent().find(".slick-prev").trigger("click");
		return false;
	});

	$(".js-next").on("click", function() {
		$(this).parent().find(".slick-next").trigger("click");
		return false;
	});

	$(".js-pop-slider").slick({
		infinite: true,
		slidesToShow: 1,
		dots: false,
		arrows: true,
		centerMode: true,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 4000
	});

	$("body").on("click", ".js-card", function(event) {
		alert();
		if ($(this).hasClass("is-active")) {
			$(this).removeClass("is-active")
		}
		else {
			$(".js-card").removeClass("is-active");
			$(this).addClass("is-active");
		}
		event.stopPropagation();
	});

	$("body").on("click", ".slick-dots", function(event) {
		$(".js-el").removeClass("is-active");
		event.stopPropagation();
	});

	$(".js-el").on("click", function() {
		if ($(this).hasClass("is-active")) {
			$(this).removeClass("is-active")
		}
		else {
			$(".js-el").removeClass("is-active");
			$(this).addClass("is-active");
		}
		//event.stopPropagation();
	});
	
	$(".js-card").hover(function(){
		$(this).addClass("is-active");
	},
	function(){
		$(this).removeClass("is-active");
	});
	

	
// ----------------- //
	function fixHeader() {
		var top = $(".search").outerHeight();
		var scroll = $(document).scrollTop();
		var header = $(".js-header-index");
		if (scroll >= top) {
			header.removeClass("header_index").addClass("is-fixed");
		}
		else {
			header.addClass("header_index").removeClass("is-fixed");
		}
		
	}
	function fixSort() {
		var top = $(".page-head").offset().top + $(".page-head").outerHeight();
		var scroll = $(document).scrollTop();
		var header = $(".js-header-index").outerHeight();
		if (scroll + header >= top) {
			$("body").addClass("is-fixed-sort");
		}
		else {
			$("body").removeClass("is-fixed-sort");
		}
		
	}
	if ($(".js-header-index").length) {
		fixHeader();
	}
	if ($(".js-sort-wrap").length) {
		fixSort()
	}
	$(window).resize(function() {
		if ($(".js-header-index").length) {
			fixHeader();
		}
		if ($(".js-sort-wrap").length) {
			fixSort()
		}
	});
	$(window).scroll(function() {
		if ($(".js-header-index").length) {
			fixHeader();
		}
		if ($(".js-sort-wrap").length) {
			fixSort()
		}
	});

	$('.js-masonry').masonry({
		// options
		itemSelector: '.js-masonry-item',
		stamp: '.js-masonry-stamp',
		columnWidth: 314,
		gutter: 40
	});

	$('.js-masonry-on-resize').masonry({
		// options
		itemSelector: '.js-masonry-item',
		columnWidth: 280,
		gutter: 40
	});

	function validate() {
		$('.js-validate').each(function(){
			if ($(this).length > 0) {
				$(this).validate({
					errorClass: 'has-error',
					rules: {
						username: {
							minlength: 2
						},
						any: {
							minlength: 2
						},
						password: {
							minlength: 5
						},
						confirm_password: {
							minlength: 5,
							equalTo: '#password'
						},
						email: {
							email: true
						},
						tel: {
							minlength: 2,
						},
						address: {
							minlength: 2
						},
						message: {
							minlength: 4
						},
						field: {
							required: true
						},
						// fruit: {
						//   required: true
						// }
					},
					messages: {
						firstname: 'Вас так зовут?',
						lastname: 'У вас такая фамилия?',
						fathername: 'У вас такое отчество?',
						password: {
							required: 'Введите пароль',
							minlength: 'Минимум 5 символов'
						},
						confirm_password: {
							 required: 'Пароли не совпадают',
							 minlength: 'Минимум 5 символов',
							 equalTo: 'Пароли не совпадают'
						},
						email: 'Неверный формат',
						address: 'Это Ваш адрес?',
						any: 'Заполните поле',
						company: 'Заполните поле',
						tel: {
							required: 'Заполните поле',
						},
						message: {
							required: 'Заполните поле',
							minlength: 'Заполните поле'
						}
					}
				});
			}
		});
	}
		
	validate();

	$('.js-tel').on('keyup', function(){
		var value = $(this).val();
		var re = /[^0-9,]/;
		if (re.test(value)) {
			value = value.replace(re, '');
			$(this).val(value);
		}
			// set max and min value
		if($(this).val().length < 7 || $(this).val().length > 12) {
			$(this).addClass('has-error');
		} else {
			$(this).removeClass('has-error');
		}
	});

	$('.js-promo-slider').on('init', function(slick) {
		setTimeout(function(){
			$('.js-promo-slider').parent().addClass("is-ready");
		},200);
	});
	$(".js-promo-slider").slick({
		infinite: true,
		slidesToShow: 1,
		dots: true,
		arrows: false,
		autoplay: true,
		adaptiveHeight: true,
		autoplaySpeed: 4000
	});

	$('.js-top-slider').on('init', function(slick) {
		setTimeout(function(){
			$('.js-top-slider').parent().addClass("is-ready");
		},200);
	});
	$(".js-top-slider").slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 740,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 400,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '20px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 375,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '15px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$(".js-el-slider").slick({
		infinite: false,
		slidesToShow: 1,
		dots: true,
		arrows: false,
		draggable: false,
		//lazyLoad: 'ondemand',
		swipe: false
	});
	$(".js-el-slider-touch").slick({
		infinite: true,
		slidesToShow: 1,
		dots: true,
		arrows: false,
		//lazyLoad: 'ondemand',
	});


// sort slider top
	$('.js-cat-slider a').on('click', function(){
	var el = $(this).attr("data-filter");
	  if (!$(this).hasClass("is-active")) {
		$('.js-cat-slider a').removeClass("is-active");
		$(this).addClass("is-active");
		//$('.js-top-slider').slick('slickFilter','.'+el);
		//filtered = true;
	  } else {
		$(this).removeClass("is-active");
		//$('.js-top-slider').slick('slickUnfilter');
		//filtered = false;
	  }
	  return false;
	});

	$('.js-sort-slider a').on('click', function(){
	  var el = $(this).attr("data-filter");
	  if (!$(this).parent().hasClass("is-active")) {
		$('.js-sort-slider li').removeClass("is-active");
		$(this).parent().addClass("is-active");
		//$('.js-slider-hot').slick('slickFilter','.'+el);
	  } else {
		$(this).removeClass("is-active");
		//$('.js-slider-hot').slick('slickUnfilter');
	  }
	  return false;
	});

	$(".js-search-example button").on('click', function(){
		var value = $(this).attr("data-val");
		$(".js-select-band .js-select").val(value);
		$(".js-select-band .select2-selection__placeholder").text(value).addClass("is-active");
		$(".js-select-band .select2-selection__rendered").text(value);
		return false;
	});

	$(".js-popup-trigger").on('click', function(){
		var popup = $(this).attr("data-popup");
		$("html").addClass("no-scroll");
		$("."+popup).fadeIn();
		return false;
	});

	$(".js-popup-close").on('click', function(){
		$(this).parents(".js-popup").fadeOut();
		$("html").removeClass("no-scroll");
		return false;
	});
	$('.js-article-slider').on('init', function(slick) {
		setTimeout(function(){
			$('.js-article-slider').parent().addClass("is-ready");
		},200);
	});
	$(".js-article-slider").slick({
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1
	});
	$(".js-raty").raty({
		score: function() {
			return $(this).attr('data-score');
		},
		hints: ['1', '2', '3', '4', '5'],
		path: 'img',
		starOn: 'star.svg',
		starOff: 'star-off.svg',
	});
	$('.js-raty-readonly').raty({
		readOnly: true,
		score: function() {
			return $(this).attr('data-score');
		},
		hints: ['1', '2', '3', '4', '5'],
		path:      'img',
		starOn:    'star.svg',
		starOff:   'star-off.svg',
		half: false,
		starHalf: 'star-half.svg'
	});

	$('.js-carousel').on('init', function(slick) {
		setTimeout(function(){
			$('.js-carousel').parent().addClass("is-ready");
		},200);
	});

	 $('.js-carousel').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  dots: true,
	  adaptiveHeight: true,
	  asNavFor: '.js-carousel-nav'
	});
	$('.js-carousel-nav').slick({
	  slidesToShow: 9,
	  slidesToScroll: 1,
	  asNavFor: '.js-carousel',
	  dots: false,
	  arrows: true,
	  adaptiveHeight: true,
	  centerMode: false,
	  focusOnSelect: true,
	  responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 5,
				}
			}
		]
	});
	$(".js-fancybox").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		padding: 50,
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 108,
				height	: 108
			}
		}
	});

	$('.js-fancybox-media').fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		padding: 0,
		helpers	: {
			media: {},
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 108,
				height	: 108
			}
		},
		afterLoad: function(current, previous) {
			console.info( 'Current: ' + current.href );        
			console.info( 'Previous: ' + (previous ? previous.href : '-') );
			
			if (previous) {
				console.info( 'Navigating: ' + (current.index > previous.index ? 'right' : 'left') );     
			}
		}
	});

	$(".js-fancybox-trigger").on('click', function(){
		$(this).parent().children().first().find("a").trigger("click");
		return false;
	});

	function uiSlider() {
		$(".js-ui-slider").each(function(){
			var slider = $(this).find(".js-ui-slider-main"),
				inputFrom = $(this).find(".js-ui-slider-from"),
				inputFromHidden = $(this).find(".js-input-from-hidden"),
				inputTo = $(this).find(".js-ui-slider-to"),
				inputToHidden = $(this).find(".js-input-to-hidden"),
				maxVal = +slider.attr("data-max"),
				minVal = +slider.attr("data-min"),
				valFrom = inputFromHidden.val(),
				valTo = inputToHidden.val(),
				stepVal = +slider.attr("data-step");
				if (!valFrom) {
					var valFrom = minVal;
				}
				if (!valTo) {
					var valTo = maxVal;
				}
			slider.slider({
				range: true,
				min: minVal,
				max: maxVal,
				step: stepVal,
				values: [ valFrom, valTo ],
				slide: function( event, ui ) {
					$(this).find(".ui-slider-handle").html("<span></span>");
					var handle_0 = $(this).find(".ui-slider-range").next().find("span")
					var handle_1 = $(this).find(".ui-slider-range").next().next().find("span");
					//inputFrom.val(ui.values[0]);
					inputFromHidden.val(ui.values[0]);
					//inputTo.val(ui.values[1]);
					inputToHidden.val(ui.values[1]);
					handle_0.text(ui.values[0]+" €");
					handle_1.text(ui.values[1]+" €");
					$(".js-sort-form .js-btn-reset").removeAttr("disabled");
				}
			});
			//console.log(handle_0);
			//console.log(handle_1);
			$(this).find(".ui-slider-handle").html("<span></span>");
			var handle_0 = $(this).find(".ui-slider-range").next().find("span")
			var handle_1 = $(this).find(".ui-slider-range").next().next().find("span");
			handle_0.text(slider.slider( "values", 0) + " €");
			handle_1.text(slider.slider( "values", 1) + " €");
			inputFromHidden.val(slider.slider( "values", 0));
			inputToHidden.val(slider.slider( "values", 1));
		});
	}
	uiSlider();

	$(".js-sort-form input").on("change", function() {
		if ($(this).parents(".js-sort-form").find("input:checked").length > 0) {

			$(".js-sort-form .js-btn-reset").removeAttr("disabled");
		}
	});
	$(".js-btn-reset").on("click", function() {
		var min = $(".js-ui-slider-main").attr("data-min");
		var max = $(".js-ui-slider-main").attr("data-max");
		var slider = $(".js-ui-slider-main");
		$(this).attr("disabled", "disabled");
		slider.slider( "values", [ min, max ] );
		slider.find(".ui-slider-range").next().find("span").text(min+" €");
		slider.find(".ui-slider-range").next().next().find("span").text(max+" €");
		$(".js-sort-form")[0].reset();
	});

	// $('.js-collapsible').collapsible({
	// 	accordion : true 
	// });

	$(".js-track-slider").slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			}
		]
	});
	function tabsLoad() {
	   $(".js-tabs-simple").each(function(){
		 if ($(this).find(".is-active").length) {
			var index = $(this).find(".is-active").index();
			$(this).next().find(".js-tabs-simple-content").eq(index).show();
		 }
		 else {
		   $(this).find("li").eq(0).addClass("is-active");
		   $(this).next().find(".js-tabs-simple-content").eq(0).show();
		 }
	   });
	}
	tabsLoad();
	$('.js-tabs-simple a').on("click",function () {
		var tabs = $(this).parents(".js-tabs-simple");
		var tabsCont = tabs.next().find(".js-tabs-simple-content");
		var index = $(this).parent().index();
		tabs.find("li").removeClass("is-active");
		$(this).parent().addClass("is-active");
		tabsCont.hide();
		tabsCont.eq(index).show();
		return false;
	});
})();